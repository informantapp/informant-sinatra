### 1.3.0

* Drop support for ruby 2.6 since it has reached EOL

### 1.2.0

* Fix activemodel deprecation warnings around errors array.

### 1.1.0

* Fix diagnostic rake task (#1)
