require 'spec_helper'

shared_examples 'ActiveRecord save action' do
  context 'with an invalid model' do
    let(:model) { User.new }

    before do
      InformantCommon::Client.start_transaction!('POST')
      model.public_send(save_action)
    end

    it 'stores the model' do
      expect(InformantCommon::Client.current_transaction.models.first.id).to eq(model.object_id)
    end
  end

  context 'with a valid model' do
    let(:model) { User.new(name: 'Han Solo', email: 'han@starwars.com') }

    before do
      InformantCommon::Client.start_transaction!('POST')
      model.public_send(save_action)
    end

    it 'gets called' do
      expect(InformantCommon::Client.current_transaction.models.first.id).to eq(model.object_id)
    end
  end
end

RSpec.describe 'Model Validation', depends_on: :active_record do
  before { InformantCommon::Client.start_transaction!('POST') }

  after { InformantCommon::Client.reset_transaction! }

  let(:request) { InformantCommon::Client.current_transaction }
  let(:model) { request.models.first }

  describe 'save actions' do
    describe '#save' do
      let(:save_action) { :save }

      it_behaves_like 'ActiveRecord save action'
    end

    describe '#valid?' do
      let(:save_action) { :valid? }

      it_behaves_like 'ActiveRecord save action'
    end

    describe '#invalid?' do
      let(:save_action) { :invalid? }

      it_behaves_like 'ActiveRecord save action'
    end
  end

  describe 'no validation errors' do
    before { User.new(email: 'test@example.com', name: 'Test').valid? }

    it 'stores the successful validation' do
      expect(model.name).to eq('User')
      expect(model.errors).to be_empty
    end
  end

  describe 'standard field validations' do
    before { User.new.valid? }

    let(:email_error) { model.errors.first }
    let(:name_error) { model.errors.last }

    it 'stores the unsuccessful validation' do
      expect(model.name).to eq('User')
      expect(model.errors.length).to eq(2)
    end

    it 'has the correct metadata for the email error' do
      expect(email_error.name).to eq('email')
      expect(email_error.value).to be_nil
      expect(email_error.message).to eq("can't be blank")
    end

    it 'has the correct metadata for the name error' do
      expect(name_error.name).to eq('name')
      expect(name_error.value).to be_nil
      expect(name_error.message).to eq('is too short (minimum is 2 characters)')
    end
  end

  describe 'custom base validations' do
    before { User.new(email: 'test@example.com', name: 'Test', add_base_error: true).valid? }

    let(:base_error) { model.errors.first }

    it 'stores the unsuccessful validation' do
      expect(model.name).to eq('User')
      expect(model.errors.length).to eq(1)
    end

    it 'has the correct metadata for the base error' do
      expect(base_error.name).to eq('base')
      expect(base_error.value).to be_nil
      expect(base_error.message).to eq('This is a base error')
    end
  end
end
