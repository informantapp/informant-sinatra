require 'bundler/setup'
require 'webmock/rspec'
require 'sinatra'

Bundler.require

class TestSuiteCapabilities
  def self.active_record?
    @active_record ||= defined?(ActiveRecord)
  end

  def self.mongoid?
    @mongoid ||= defined?(Mongoid)
  end

  def self.enabled?(capability)
    public_send("#{capability}?")
  rescue StandardError
    true
  end
end

ENV['INFORMANT_API_KEY'] = 'abc123'
require 'informant-sinatra'

Dir[File.expand_path(File.join(File.dirname(__FILE__), 'support', '**', '*.rb'))].sort.each { |f| require f }

RSpec.configure do |config|
  config.before do
    TestMigration.up if TestSuiteCapabilities.active_record?

    InformantCommon::Config.reset!
  end

  config.around do |example|
    example.run if TestSuiteCapabilities.enabled?(example.metadata[:depends_on])
  end
end
