require 'spec_helper'

describe InformantSinatra::Bootstrap do
  describe '.registered' do
    let(:_app) { double }

    context 'when the agent is enabled' do
      it 'activates the trackers' do
        allow(described_class).to receive(:transmit_agent_info)
        allow(_app).to receive(:use)
        allow(described_class).to receive(:register_validation_trackers)

        described_class.registered(_app)

        expect(described_class).to have_received(:transmit_agent_info)
        expect(_app).to have_received(:use).with(InformantSinatra::Middleware)
        expect(described_class).to have_received(:register_validation_trackers)
      end
    end

    context 'when the agent is disabled' do
      before { InformantSinatra::Config.api_token = '' }

      it 'does not activate anything' do
        allow(described_class).to receive(:transmit_agent_info)
        allow(_app).to receive(:middleware)
        allow(described_class).to receive(:register_validation_trackers)

        described_class.registered(_app)

        expect(described_class).not_to have_received(:transmit_agent_info)
        expect(_app).not_to have_received(:middleware)
        expect(described_class).not_to have_received(:register_validation_trackers)
      end
    end
  end

  describe '.transmit_agent_info' do
    let(:agent_info_json) do
      InformantCommon::Event::AgentInfo.new(
        agent_identifier: "informant-sinatra-#{InformantSinatra::VERSION}",
        framework_version: "sinatra-#{Sinatra::VERSION}"
      ).to_json
    end

    let!(:agent_info_request_stub) do
      stub_request(:post, 'https://collector-api.informantapp.com/v2/client-info')
        .with(body: agent_info_json)
    end

    before do
      described_class.transmit_agent_info.join
    end

    it 'sends the agent info' do
      expect(agent_info_request_stub).to have_been_requested
    end
  end
end
