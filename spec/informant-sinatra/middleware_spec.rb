require 'spec_helper'

describe InformantSinatra::Middleware do
  describe '#call' do
    let(:middleware) { described_class.new(app) }
    let(:app) { double }
    let(:env) { { 'REQUEST_METHOD' => 'POST', 'sinatra.route' => '/what/a/great/route' } }
    let(:response) { double }

    before do
      allow(InformantCommon::Client).to receive(:start_transaction!)
      allow(app).to receive(:call).and_return(response)
      allow(InformantCommon::Client).to receive(:process)
    end

    it 'wraps the request in an informant transaction' do
      expect(middleware.call(env)).to eq(response)
      expect(InformantCommon::Client).to have_received(:start_transaction!).with('POST')
      expect(app).to have_received(:call).with(env)
      expect(InformantCommon::Client).to have_received(:process)
      expect(InformantCommon::Client.current_transaction.handler).to eq('/what/a/great/route')
    end
  end
end
