require 'informant-common'

module InformantSinatra
  autoload :Bootstrap,          'informant-sinatra/bootstrap'
  autoload :Config,             'informant-sinatra/config'
  autoload :Diagnostic,         'informant-sinatra/diagnostic'
  autoload :Middleware,         'informant-sinatra/middleware'
  autoload :ValidationTracking, 'informant-sinatra/validation_tracking'
  autoload :VERSION,            'informant-sinatra/version'
end
