module InformantSinatra
  class Middleware
    def initialize(app)
      @app = app
    end

    def call(env)
      InformantCommon::Client.start_transaction!(env['REQUEST_METHOD'])
      response = @app.call(env)
      InformantCommon::Client.current_transaction.handler = env['sinatra.route']
      InformantCommon::Client.process
      response
    end
  end
end
