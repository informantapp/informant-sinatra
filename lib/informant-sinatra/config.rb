module InformantSinatra
  module Config
    def self.api_token
      InformantCommon::Config.api_token
    end

    def self.api_token=(api_token)
      InformantCommon::Config.api_token = api_token
    end

    def self.enabled?
      InformantCommon::Config.enabled?
    end

    def self.exclude_models
      InformantCommon::Config.exclude_models
    end

    def self.exclude_models=(exclude_models)
      InformantCommon::Config.exclude_models = exclude_models
    end

    def self.filter_parameters
      InformantCommon::Config.filter_parameters
    end

    def self.filter_parameters=(filter_parameters)
      InformantCommon::Config.filter_parameters = filter_parameters
    end

    def self.value_tracking?
      InformantCommon::Config.value_tracking?
    end

    def self.value_tracking=(value_tracking)
      InformantCommon::Config.value_tracking = value_tracking
    end
  end
end
