module InformantSinatra
  module Bootstrap
    def self.registered(app)
      return unless InformantSinatra::Config.enabled?

      transmit_agent_info

      InformantSinatra::Config.filter_parameters = %w[password token] if InformantSinatra::Config.filter_parameters.empty?

      app.use InformantSinatra::Middleware

      register_validation_trackers
    rescue StandardError => e
      puts "Unable to bootstrap informant: #{e.message}"
    end

    def self.transmit_agent_info
      InformantCommon::Client.transmit(
        InformantCommon::Event::AgentInfo.new(
          agent_identifier: "informant-sinatra-#{InformantSinatra::VERSION}",
          framework_version: "sinatra-#{Sinatra::VERSION}"
        )
      )
    end

    def self.register_validation_trackers
      return unless defined?(ActiveSupport)

      ActiveSupport.on_load(:active_record) do
        include InformantSinatra::ValidationTracking
      end

      ActiveSupport.on_load(:mongoid) do
        include InformantSinatra::ValidationTracking
      end
    end
  end
end
