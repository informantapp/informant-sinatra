module InformantSinatra
  class Diagnostic
    def self.run
      new.run
    end

    def run
      if Config.enabled?
        response = InformantCommon::Client.transmit(test_form_submission).join.value
        display_response_message(response)
      else
        puts missing_api_token_message
      end

      puts assistance_message
    end

    private

    def missing_api_token_message
      %(
        Your API token could not be found in the configuration. You can retrieve it from your Informantapp.com dashboard.

          Then add it to your server's environment as `INFORMANT_API_KEY`

          OR

          Set it manually in your application setup

            InformantSinatra::Config.api_token = 'your token'
      )
    end

    def bad_response_message(server_message)
      "Seems like we have a problem. \"#{server_message}\" in this case."
    end

    def success_message
      'Everything looks good. You should see a test request on your dashboard.'
    end

    def assistance_message
      "If you need assistance or have any questions, send an email to support@informantapp.com or tweet @informantapp and we'll help you out!"
    end

    def test_form_submission
      InformantCommon::Event::FormSubmission.new.tap do |form_submission|
        form_submission.handler = 'Connectivity#test'
        form_submission.process_model(
          InformantCommon::Model::Base.new(
            name: 'TestClass',
            field_errors: [InformantCommon::FieldError.new('field_name', nil, "can't be blank")]
          )
        )
      end
    end

    def display_response_message(response)
      if response.code == '204'
        puts success_message
      else
        puts bad_response_message(response.body)
      end
    end
  end
end
