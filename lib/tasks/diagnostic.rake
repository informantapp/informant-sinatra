require 'informant-sinatra'
require 'json'

namespace :informant do
  desc 'Verify connectivity from your app to Informant'
  task :diagnostic do
    InformantSinatra::Diagnostic.run
  end
end
