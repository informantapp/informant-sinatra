# Overview

## [![Homepage](https://s3.amazonaws.com/assets.heroku.com/addons.heroku.com/icons/1347/original.png)](https://www.informantapp.com) InformantSinatra

The `informant-sinatra` gem provides Sinatra hooks for The Informant.

## Compatibility

The informant-sinatra gem officially supports Ruby 2.5+.

It will work automatically with Sinatra 2+ and automatically activate
validation tracking for ActiveRecord and Mongoid models.

[![pipeline status](https://gitlab.com/informantapp/informant-sinatra/badges/master/pipeline.svg)](https://gitlab.com/informantapp/informant-sinatra/-/commits/master)

## Installation

1. Sign up for an account on https://www.informantapp.com or through [Heroku](https://dashboard.heroku.com).
2. Add the `informant-sinatra` gem to your `Gemfile`.

   ```
   gem 'informant-sinatra', group: :production
   ```

3. Verify the `INFORMANT_API_KEY` is set in your app's environment.
4. Bootstrap the Informant in your app's setup.

   ```
   class MySinatraApp < Sinatra::Base
     register InformantSinatra::Bootstrap
   ```

5. Deploy with the gem installed.
6. Submit a form and you'll see it appear in [our web interface](https://console.informantapp.com).

We've set up
[informant-sinatra-example](https://gitlab.com/informantapp/informant-sinatra-example)
as a demonstrate of installation and operation.

## Troubleshooting

The Informant gem includes a rake task to diagnose connectivity problems. From
your server environment execute `rake informant:diagnostic` and it will print
helpful information to the terminal.

Note: Sinatra doesn't load rake tasks from gems by default so you'll need to
have code in your Rakefile to do it.

## Usage

By default, any request that causes an ActiveRecord or Mongoid model to be
validated will be tracked by the Informant once the gem is added to your
project.

### Custom Tracking

If you have other objects that conform to the ActiveModel interface, you can
include validation tracking in your class and it will just work.

```
class VeryActiveModel
  include ActiveModel::Model
  include InformantSinatra::ValidationTracking
end
```

## Configuration

Configuration options are specified in the `InformantSinatra::Config` module.
Reasonable defaults are provided but you can override them as necessary by
creating an initializer and passing a block to `configure`.

```
InformantSinatra::Config.configure do |config|
  config.api_token = ENV['INFORMANT_API_KEY']
  config.exclude_models = %w(UntrackedModel)
  config.filter_parameters.push *%w(password password_confirmation)
  config.value_tracking = true
end
```

### api_token

Default Value: `ENV['INFORMANT_API_KEY']`

Example Value: `dff67d9e61eaa8cf110b3d3f3238a671`

This should be set to the API key assigned to you when you provisioned your application. Heroku will automatically add this to your environment when you provision the addon. Otherwise, you can get this from the Informant web application.

### exclude_models

Default Value: `[]`

Example Value: `['Person', 'Employee']`

This allows you to exclude certain models from tracking. If you don't want them
included in your metrics list their class names as strings here.

### filter_parameters

Default Value: `['password', 'token']`

Example Value: `['password', 'token']`

Any field values with names matching the strings in this array will be excluded
from tracking.  Sensitive information you wouldn't want to include in your
server log should be listed here as well.

### value_tracking

Default Value: `true`

Example Value: `false`

This will enable and disable tracking of values globally. If you turn this off,
no field value information will be tracked at all. This is useful if you have
compliance or security concerns about the nature of your data.
